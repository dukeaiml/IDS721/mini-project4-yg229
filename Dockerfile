# Use an official Rust image
FROM rust:1.75 as builder
# Create a new empty shell project
RUN USER=root cargo new --name actix_web_service actix_web_service
WORKDIR /actix_web_service

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# Get the dependencies cached
RUN cargo build --release
# Remove the dummy source files
RUN rm src/*.rs

# Copy source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/actix_web_service*
RUN cargo build --release

# Final stage
FROM debian:bookworm-slim
# Copy the compiled binary from the builder stage
COPY --from=builder /actix_web_service/target/release/actix_web_service .
RUN chmod +x ./actix_web_service
# Run the binary
CMD ["./actix_web_service"]


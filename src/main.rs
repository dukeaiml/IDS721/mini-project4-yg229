use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet_and_sum() -> impl Responder {
    // Define a list of numbers
    let numbers = vec![1, 2, 3, 4, 5]; 
    // Calculate the sum
    let sum: i32 = numbers.iter().sum();
    // Create a string that includes a greeting and the sum of the list
    let response = format!("Hi! this is a demo from Yanbo. The sum of the list {:?} is {}", numbers, sum);
    // Respond with the string
    HttpResponse::Ok().body(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet_and_sum)) 
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
